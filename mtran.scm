(import scheme
        uri-common
        http-client
        args
        srfi-1
        (chicken base)
        (chicken process-context)
        (chicken port)
        (chicken io))

(define src-lang "en")
(define dest-lang "ru")

(define lang-alist
  '((en . 1)
    (ru . 2)
    (de . 3)))

(define (get-code lang)
  (cdr (assoc (string->symbol lang) lang-alist)))

(define (make-query word)
  (absolute-uri (string-append "https://www.multitran.com/m.exe?s="
                               word
                               "&l1="
                               (number->string (get-code src-lang))
                               "&l2="
                               (number->string (get-code dest-lang)))))

(define (runner word)
  (print
   (with-input-from-request (make-query word) #f read-string)))

(define (usage)
  (with-output-to-port (current-error-port)
    (lambda ()
      (print "Usage: " (car (argv)) " [options ...]")
      (newline)
      (print (args:usage opts))))
  (exit 1))

(define opts
  (list (args:make-option
         (f from) #:required "Set source language."
         (set! src-lang arg))
        (args:make-option
         (t to) #:required "Set destination language."
         (set! dest-lang arg))
        (args:make-option
         (h help) #:none "Print usage information."
         (usage))))

(receive (options operands)
    (args:parse (command-line-arguments) opts)
  (define word (apply string-append (intersperse operands "+")))
  (runner word))
